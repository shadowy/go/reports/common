package common

import (
	"gopkg.in/yaml.v3"
	"io"
	"os"
)

type ReportSchema struct {
	Version string       `yaml:"version"`
	Author  *string      `yaml:"author"`
	Pages   []ReportPage `yaml:"pages"`
}

func (report *ReportSchema) String() (*string, error) {
	data, err := yaml.Marshal(report)
	if err != nil {
		return nil, err
	}
	result := string(data)
	return &result, nil
}

func (report *ReportSchema) ToWriter(writer io.Writer) error {
	data, err := report.String()
	if err != nil {
		return err
	}
	_, err = writer.Write([]byte(*data))
	return err
}

func (report *ReportSchema) ToFile(filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer func() {
		_ = file.Close()
	}()
	return report.ToWriter(file)
}
