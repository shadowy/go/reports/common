package units

import "strings"

type Unit int

const (
	Unknown Unit = iota
	MM
	CM
	Inch
	Pixels
)

var unitNames = []string{"mm", "cm", "inch", "px"}

func (unit Unit) String() string {
	if unit < MM || unit > Pixels {
		return "Unknown"
	}
	return unitNames[unit-1]
}

func (unit Unit) Parse(data string) Unit {
	for pos, name := range unitNames {
		if strings.ToLower(data) == name {
			return Unit(pos)
		}
	}
	return Unknown
}
