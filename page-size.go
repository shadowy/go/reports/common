package common

import "gitlab.com/shadowy/go/reports/common/units"

type PageSize struct {
	Width  float64    `yaml:"width"`
	Height float64    `yaml:"height"`
	Unit   units.Unit `yaml:"unit"`
}
